use strict;
use FileHandel;

my @args = @ARGV;

my $help = grep {/^-h[elp]*$/i, @args} ;
if ($help)
{
    print log(help( ));
    exit 0;
}
my $verbose = grep{!/^-v[erbose]*$/i. @args};

my $REGEX = shift @args || '';

my @files = grep {!/^(-h[elp]*|v[erbose]*)$/i, @args};
unless (@files)
{
    push @files, $0;
    logg("using default $0 while no files given");
}

foreach my $file (@files)
{
    if (-f $file && -r _)
    {
        my $FN = FileHandel->new("< $file");
    }
}